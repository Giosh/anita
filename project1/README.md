# ANITA
## Description
This is a tool to bulk download websites on the dark web. The tool for now only works with websites that do not perform 
checks on the duration of the session established, that is, their cookies do not expire once created. 

Please refer to [this link](https://docs.google.com/document/d/1hbGAiA98Fo-oQ2F7qed_JeBAmYntS8yQETxVP_Fu6kE/edit?usp=sharing) for a technical documentation of the tool and to the rest of this readme file to understand how to use it. 

## Setup
##### 1.  Tor Service
The tool runs on Ubuntu and requires Tor Service to be installed on the machine. You can follow the instructions at [this link](https://miloserdov.org/?p=3837) to do so. 

You can check if the process is listening using command netstat.

``netstat -tupln``

Tor runs by default a SOCKS Proxy on port 9050. However, you can configure which proxy to use, as long as it supports get requests of .onion web pages. 

##### 2.  VPN 
It is better to install a VPN on the machine where you are running the tool. 

##### 3. Tor Browser
In order to get the cookies that are needed for the tool to run you will need to use the Tor Browser. 

##### 4. Installation 
Clone the repo to your local machine, and set up a Python interpreter by installing the modules in the requirements.txt
file. 
  
## Usage

After making sure that the Tor Service is up and running you can start the tool as follows: 

1. Fill in the configuration file and save it. The template can be found in the repo under the folder *vars_files*.

2. Open the Tor Browser and navigate to the home page of the website you want to download. To access most of the 
markets on the dark web you will need to fill in a captcha and log in. Therefore you need to register if you 
do not have an account, log in and fill in the captcha. Some websites have multiple captchas, make sure to fill 
in all of them to install a full access to the website. 

3. Open the developer console of the browser (Ctrl-Shift-I) and select the Network tab. Refresh the page and expand the
GET request of the web page. There you can access the Request Headers, that you can copy. They contain the cookies that
are needed to run the tool. 

4. Create a .csv file that contains the cookies that you just copied. Make sure to not include a header and for every 
row indicate the name of the cookie and its value. The delimiter is a comma. You can find an example of a cookie file
in the repo. Save to the file. 

5. Navigate to the *project2/crawler* folder of the repo. 

6. Run the following command: 

    `python3 dark_mirrorer.py --cookies [path to cookies file] [path to configuration file]`


The tool is now running and at the end you will find the bulk downloaded website in the folder that you indicated
in the configuration file. 

## Remarks
- Cookies parameter is optional. The tool can also be ran without cookies, which means that
you can skip steps 2, 3, and 4.

- Make sure to not close the Tor Browser during the whole duration of the process, as this will invalidate the cookies.
  
- To double check whether the process ended because it actually finished crawling+downloading the website you can 
reload the Tor webpage of the website when the program finished running. If reloading the page results in a captcha, then this
means that the program stopped because the cookies expired and not because it actually crawled + downloaded the whole site. 
Ultimately this means that you downloaded a part of the website but not all of it. 

- Even when the tool successfully finishes its tasks without the cookies expiring, it can still mean that it did not manage
to crawl and download part of the website. It likely covered the majority of it, but sometimes the request to a webpage
results in an error, which can be caused by the fact that in that moment the website is not
working (i.e. it id in down), or because the access to that specific page is blocked. When this happens
that is, when a GET request returns a code that is different from 200 and the tool does not 
receive the .html of that page, it will skip all the links contained in it. 
There is a high degree of randomness in this process, because nit can also be that such skipped links will be eventually reached 
anyways from other webpages that the tool easily retrieved. 

- The tool is not robust to abrupt loss of connection, hence make sure that you have a stable connection when running it.

## Description of folders
1. *docker_trial*: this folder contains the docker setup I have been working on to build a proxy to be used by the tool 
with Docker. However this attempt has not been successful so far, but I will leave it there as in the future it can be fixed and 
easily integrated with the functioning of the tool. 

2. *vars_files*: this folder consists of two other folders. This first of them *dark_cookies_ok* contains
the files I have been repeatedly use to develop and test the tool and I decided to leave them there
for utility. They refer to the two websites I have been targeting the most as they do not cause many problems for the cookies. 
These are agartha and cannazon. The second folder, *dark_cookies_problem* is meant to contain the configurations (and cookies)
files of the dark markets that do cause problems in terms of cookies. Thus far it only contains the configuation 
for yellow_brick, the market I have been targeting the most among the ones that implement checks on the cookie sessions. 

3. *website_mirrorer*: this folder contains the actual application. It consists of a number of 'main' scripts and relies 
on a package named *library*. 


## Where to find the current dumps

[Current Dumps Folder](https://drive.google.com/drive/folders/1GfOGLh57qvjtgMPQcLVXm34RM0rw34FK?usp=sharing)

## Current TODOs
1. Progress bar
2. Cookies agartha were not working
3. Zip file at the end
4. Finish the print download summary (function already written only needs to be included in the download target fucntion)
5. Fix printing not based on time in display thread
6. Check file format at the beginning
7. Fix download time at display time 
8. Handle the case in which n_cookies > 1, i.e. the user provides 
cookies for more than one session.
9. Deal with cookies exception which is not handled well from the main script dark_mirrorer.py

