from library import check_cookie_session as cookie_check, scrape_links as lnk, vars as vr, utils as utl
import time as t
from urllib.parse import urlparse


def start_crawler(event):
    """
    This procedure is responsible for the crawling of the website. In the beginning
    it checks whether the cookies actually work and then it starts the process. When it is
    finished it saves the results and some statistics.
    :param event: Since the downloading thread can start only after the crawling one successfully
    started, this event is necessary for the threads to be able to synchronize
    with each other.
    :return: None- procedural routine.
    """
    # ---------------------------------------------------- Initialize

    # initialize useful variables for crawling
    domain_name = urlparse(utl.get_config(utl.get_config(vr.internal_config_file, vr.path_to_config), vr.url)).netloc
    direct = utl.get_direct()

    # -------------------------------------------------- Check if cookies are working
    if not cookie_check.check_cookie_session():
        utl.log_event(vr.crawl_log_file, vr.crawler, 'Cookies are not working and we cannot connect to the website :(')
        raise Exception('Cookie Session is not working... Please enter new cookies or re-try later')

    # ------------------------------------------------ Init internal config file with the crawling start time
    utl.update_config(vr.crawler + vr.internal_config_file, **{vr.crawl_start: t.time()})

    # --------------------------------------------------Set sync event to notify downloader
    event.set()

    # -------------------------------------------------- Start Crawling
    # Get path to general configuration file
    path_to_config = utl.get_config(vr.internal_config_file, vr.path_to_config)

    # crawl from url
    links = set()
    lnk.crawl(utl.get_config(path_to_config, vr.url), links, direct + vr.links_file, direct + vr.all_links_file,
              direct + vr.crawl_log_file, utl.get_config(vr.crawler + vr.internal_config_file, vr.crawl_start),
              utl.get_config(path_to_config, vr.max_crawl_time), utl.get_config(path_to_config, vr.delay),
              utl.get_config(path_to_config, vr.n_cookies), utl.get_cookies(),
              utl.get_config(path_to_config, vr.user_agent), utl.get_config(path_to_config, vr.http_proxy),
              utl.get_config(path_to_config, vr.max_depth), domain_name)

    # Calculate crawling time
    end = t.time()
    total_time = (end - utl.get_config(vr.crawler + vr.internal_config_file, vr.crawl_start)) / 60

    # --------------------------------------------------Clear sync event to notify downloader
    event.clear()

    # ---------------------------------------------------- Write to files crawling results and statistics
    try:
        lnk.calculate_n_links_per_l_depth(direct + vr.links_file, direct + vr.freq_crawled_per_depth)
        lnk.crawl_summary(direct + vr.links_file, direct + vr.all_links_file, direct + vr.crawl_summary)
        crawl_summary_file = utl.safe_open_w(direct + vr.crawl_summary, 'a')
        print('Total crawling time (in minutes): ', total_time, file=crawl_summary_file)
        crawl_summary_file.close()
    except Exception as e:
        utl.log_event(vr.crawl_log_file, vr.crawler, str(e))
