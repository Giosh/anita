from library import save_webpage as sw, vars as vr, utils as utl
import pandas as pd
import os.path
import time as t


# TODO Manage versioning of the saved websites: zip file with date?
# just change directory name from agartha to argartha+date, or leave agartha
# and then withing agartha version all the downloads?
# zip file must contain the list of the crawled websites or just the website?
# TODO fix the .jpj.html
# TODO fix percentage thing with downloading images
# TODO  add an overall overview of the downloading percentage of the crawled links
# TODO zip files in the end

def start_download(crawl_event, download_event):
    # ------------------------------------------------------- Wait for the crawler to start
    utl.log_event(vr.download_log_file, vr.downloader, 'Blocking Downloader until Crawler starts')
    crawl_event.wait()

    # --------------------------------------------------- Set download event, save starting time and log download start
    download_event.set()
    utl.update_config(vr.downloader + vr.internal_config_file, **{vr.download_start: t.time()})
    utl.log_event(vr.download_log_file, vr.downloader, 'Crawler Started --> Starting Downloader')

    # ------------------------------------------------- Get path to config
    path_to_config = utl.get_config(vr.internal_config_file, vr.path_to_config)

    # -------------------------------------------------------- Get Download directory
    direct = utl.get_direct()

    # -------------------------------------------------- As long as the crawler is running download the crawled links

    while crawl_event.isSet():
        # Update download time
        utl.update_config(vr.downloader + vr.internal_config_file, **{vr.download_time: (t.time() - utl.get_config(
            vr.internal_config_file, vr.download_start)) / 60})

        # Check if a file with the downloaded links exists already and load the links that have been downloaded already
        if os.path.isfile(direct + vr.downloaded_links):
            with open(direct + vr.downloaded_links) as f:
                downloaded_links = f.read().split('\n')
        else:
            downloaded_links = []

        # Read crawled inks so far
        try:
            links_df = pd.read_csv(direct + vr.links_file)
            links_df.columns = ['url', 'depth', 'response_code']
            crawled_links = links_df.url
        except Exception as e:
            utl.log_event(vr.download_log_file, vr.downloader, str(e))
            continue

        # Download crawled links that are not downloaded yet
        for crawled_link in crawled_links:
            if crawled_link not in downloaded_links:
                sw.get_and_save_webpage(crawled_link, direct, utl.get_config(path_to_config, vr.user_agent),
                                        utl.get_config(path_to_config, vr.http_proxy),
                                        utl.get_config(path_to_config, vr.n_cookies), utl.get_cookies())
                # Save downloaded link to file and update downloaded_links counter
                try:
                    with utl.safe_open_w(direct + vr.downloaded_links, 'a') as f1:
                        f1.write(crawled_link)
                        f1.write('\n')
                    utl.update_config(vr.downloader + vr.internal_config_file,
                                      **{vr.n_downloaded_links: len(downloaded_links) + 2})
                except Exception as e:
                    utl.log_event(vr.download_log_file, vr.downloader, str(e))
                    continue

    # Save summaries
    download_time = (t.time() - utl.get_config(vr.internal_config_file, vr.download_start)) / 60
    sw.download_summary(direct + vr.downloaded_links, direct + vr.download_summary, download_time)
    # Clear download event to signal download end
    download_event.clear()
