# ------------ Thread names
main = 'dark_mirrorer'
crawler = 'crawler'
downloader = 'downloader'
# ------------ Parameters names (internal configs)
path_to_config = 'path_to_config'
path_to_cookies = 'path_to_cookies'
timestamp = 'timestamp'
crawl_start = 'crawl_start'
download_start = 'download_start'
crawl_time = 'crawl_time'
download_time = 'download_time'
current_depth = 'current_depth'
n_crawled_links = 'n_crawled_links'
n_downloaded_links = 'n_downloaded_links'
# ------------ Parameters names (configs)
url = 'url'
http_proxy = 'http_proxy'
user_agent = 'user_agent'
n_cookies = 'n_cookies'
max_crawl_time = 'max_crawl_time'
delay = 'delay'
max_depth = 'max_depth'
download_dir = 'download_dir'
market_name = 'market_name'
# ------------ File names
# Internal configuration file
internal_config_file = 'internal_configs.csv'
# file containing the unique crawled list of links of a website
links_file = 'onion_links.csv'
# file containing all the crawled list of links of a website
all_links_file = 'onion_all_links.txt'
# file containing all the downloaded links from the crawled links
downloaded_links = 'onion_downloaded_links.txt'
# crawl summary file
crawl_summary = 'crawl_Summary.txt'
# download summary file
download_summary = 'download_Summary.txt'
# file containing frequency data of crawled links
freq_crawled = 'onion_freq_crawled.txt'
# file containing the counts of crawled links per level of depth
freq_crawled_per_depth = 'onion_freq_crawled_per_depth.csv'
# global log file
global_log_file = 'global_log_file.txt'
# crawl log file
crawl_log_file = 'crawl_log_file.txt'
# download log file
download_log_file = 'download_log_file.txt'
