import requests
from . import utils as utl
from . import vars as vr


def check_cookie_session():
    """
    This routine tests whether the provided cookies are actually working or not. It simply tries a
    GET requests to check whether the response code is 200. If the cookies are not working it returns FALSE
    and the tool never starts the crawling+downloading of the website.
    :return: True/False depending on whether the cookies are working or not.
    """
    # Get path tho the user-provided configuration file
    path_to_config = utl.get_config(vr.internal_config_file, vr.path_to_config)

    # initialize a session object, set the user agent and the proxies.
    s = requests.Session()
    s.headers.update({
        'User-Agent': utl.get_config(path_to_config, vr.user_agent)
    })
    s.proxies['http'] = utl.get_config(path_to_config, vr.http_proxy)
    s.proxies['https'] = utl.get_config(path_to_config, vr.http_proxy)

    # Case in which there is only one dict of cookies provided
    if utl.get_config(path_to_config, vr.n_cookies) == 0:
        try:
            r = s.get(utl.get_config(path_to_config, vr.url))
            if r.status_code != 200:
                return False
        except Exception as e:
            utl.log_event(vr.crawl_log_file, vr.crawler, str(e))
            return False
    else:
        # Case in which multiple dicts of cookies are provided
        try:
            responses = []
            for cookies in utl.get_cookies():
                r = s.get(utl.get_config(path_to_config, vr.url), cookies=cookies)
                responses.append(r.status_code)
            for r in responses:
                if r != 200:
                    return False
        except Exception as e:
            utl.log_event(vr.crawl_log_file, vr.crawler, str(e))
            return False
    return True
