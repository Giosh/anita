import datetime as d
import errno
import os
import csv
import os.path
import zipfile
import pandas as pd
from pandas.errors import EmptyDataError
import ast

from django.core.validators import URLValidator
from django.core.exceptions import ValidationError

from . import vars as vr


# Taken from https://stackoverflow.com/a/600612/119527
def mkdir_p(path):
    """ create the path"""
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def safe_open_w(path, mode):
    """ Open "path" for writing, creating any parent directories as needed.
    """
    mkdir_p(os.path.dirname(path))
    return open(path, mode)


def zipper(path, zipname):
    """
    zip the folder in path using the zipname
    :param path: path of the folder
    :param zipname: name to give to the zip file
    :return: nothing to return
    """
    # ziph is zipfile handle
    zip_h = zipfile.ZipFile(zipname, 'w', zipfile.ZIP_DEFLATED)
    for root, dirs, files in os.walk(path):
        for file in files:
            zip_h.write(os.path.join(root, file))
    zip_h.close()


# TODO this needs to be finished. It is supposed to be called at the beginning of the execution of the main
# script (dark_mirrorer.py) to check that the files at the paths specified from the used comply with the format
# that the tool is expecting.
def check_config_file_format(config_file):

    configs = [vr.url, vr.http_proxy, vr.user_agent, vr.delay, vr.max_depth, vr.max_crawl_time, vr.n_cookies,
               vr.market_name, vr.download_dir]
    config_df = pd.read_csv(config_file, header=None, index_col=0)

    # Check first if all the required fields are present
    missing_configs = []
    for config in configs:
        if config not in list(config_df.index.values):
            missing_configs.append(config)
    if len(missing_configs) != 0:
        msg = 'Missing Configuration Fields: '
        for config in missing_configs:
            msg = msg + ' ' + config
        raise Exception(msg)

    # Check if all the fields have the right type
    val = URLValidator(verify_exists=False)
    try:
        val(config_df.at[vr.url, 1])
    except ValidationError as e:
        raise Exception(str(e))


def create_int_config_file(filename):
    """
    This tool uses three different internal configuration files that are created and modified at run time, and are
    specific to each execution, hence to each website and moment in time. This routine creates such file with
    the given file name.
    Note: I call them internal configuration file to distinguish them from the configuration file provided by
    the user at the beginning of the execution of the tool.
    :param filename: name of the internal configuration file to be created.
    :return: no return given
    """
    int_config_file = open(filename, 'w', newline='')
    csv_writer = csv.writer(int_config_file, delimiter=',')
    csv_writer.writerow(['NAME', 'VALUE'])
    int_config_file.close()


def get_config(config_file, param):
    """
    Returns the value of the param in the specified config_file.
    Note: the configuration files used in the scope of this project are always in the format of csv files.
    Each row in the cvs files is composed by the name of the parameter followed by the value of such
    parameter.
    :param config_file: configuration file
    :param param: parameter name
    :return: parameter value or 0 if the parameter of file does not exist.
    """
    try:
        config_df = pd.read_csv(config_file, header=None, index_col=0)
        value = config_df.at[param, 1]
        if value.replace('.', '', 1).isdigit():
            value = ast.literal_eval(value)
        return value
    except Exception as e:
        # Todo handle this one properly
        return 0


def update_config(config_file, **kwargs):
    """"
    This routine updates the values specified in the kwargs dict in the specified config_file"""
    try:
        config_df = pd.read_csv(config_file, header=None, index_col=0)
    except EmptyDataError as e:
        log_event(vr.global_log_file, vr.main, str(e))
        config_df = pd.DataFrame()
    for key, value in kwargs.items():
        config_df.at[key, 1] = value
    config_df.reset_index(inplace=True)
    config_df.to_csv(config_file, header=False, index=False)


def form_work_dir_path(download_dir, timestamp, market_name):
    """
    Forms the correct path of the directory where the tool downloads the website and saves the crawling and downloading
    files. It forms this path by merging the directory indicated by the user in the configuration file, the market name
    and the timestamp.
    :param download_dir: directory specified from the user in the configuration file.
    :param timestamp: timestamp of the moment when the tool has been initiated
    :param market_name: the name of the market/website to be crawled and downloaded.
    :return: No return
    """
    if download_dir[0] != '/':
        download_dir = '/' + download_dir
    if download_dir[-1] != '/':
        download_dir = download_dir + '/'

    date = timestamp.split(' ')[0]
    time = timestamp.split(' ')[1].split('.')[0].replace(':', '-')
    timestamp = date + 'T' + time

    path = download_dir + market_name + '/' + timestamp + '/'

    return path


def set_up_directory():
    """
    This routing creates a directory where the tool saves the website and the related crawling and downloading files.
    The directory uses the path specified in the configuration file provide by the user, and from there it creates a folder
    with the market name (if it does not exist already) and a new unique folder within this last one with the
    timestamp of the moment when the tool has been initiated.
    :return: No return
    """
    timestamp = str(d.datetime.now())
    update_config(vr.internal_config_file, **{vr.timestamp: timestamp})
    path_to_config = get_config(vr.internal_config_file, vr.path_to_config)

    path = form_work_dir_path(get_config(path_to_config, vr.download_dir), timestamp,
                              get_config(path_to_config, vr.market_name))
    try:
        os.mkdir(path)
    except OSError:
        print("Creation of the directory %s failed" % path)
    else:
        print("Successfully created the directory %s " % path)


def get_direct():
    """
    This routine returns the direct where the tool downloads the website and saves the files related to the crawling
    and downloading of the website.
    :return: No return
    """
    path_to_config = get_config(vr.internal_config_file, vr.path_to_config)
    timestamp = get_config(vr.internal_config_file, vr.timestamp)
    download_dir = get_config(path_to_config, vr.download_dir)
    market_name = get_config(path_to_config, vr.market_name)
    return form_work_dir_path(download_dir, timestamp, market_name)


def get_cookies():
    """
    This routing returns a list of dicts. Each dict contains the cookies of one session.
    Note: in the scope of this project, cookies are passed to the get requests as a dict in which each key is the name
    of the cookie and the value the corresponding cookie value. This tool was designed to handle the case in which
    the user provides multiple values for the same cookie, i.e. more than one dictionary such that multiple sessions
    can be used and hence prevent the website to block the tool.
    The user can indicate how many sessions is using with the n_cookies parameter.
    However this feature has not been tested thoroughly, since I worked and tested the tool with websites that
    did not block the get requests. However the tool is structured in such a way that implementing it should be fast.
    :return: a list of dicts.
    """
    if get_config(get_config(vr.internal_config_file, vr.path_to_config), vr.n_cookies) > 1:
        # TODO For now return None but you need to handle this eventually
        return None
    else:
        cookies = {}
        with open(get_config(vr.internal_config_file, vr.path_to_cookies), newline='') as cookies_file:
            cookies_reader = csv.reader(cookies_file, delimiter=',')
            for line in cookies_reader:
                cookies[line[0]] = line[1]
        return [cookies]


def log_event(log_file, script, message):
    """
    This routing writes a log message to a specified log file. It basically adds a row to such file in the format
    specified in the print statement.
    :param log_file: file to write the log message
    :param script: script that generates the message during the execution (can be for example the download or
    crawl thread)
    :param message: log message to write
    :return: No return
    """
    timestamp = str(d.datetime.now())
    data = dict(timestamp=timestamp, script=script, message=message)
    log_file = safe_open_w(get_direct() + log_file, 'a')

    print("{timestamp}   {script}   {message}\n".format(**data), file=log_file)
    log_file.close()
