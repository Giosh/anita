import requests
import os
from bs4 import BeautifulSoup as bs
from urllib.parse import urljoin, urlparse
from . import utils as utl, vars as vr


def is_absolute(url):
    """
    Determines whether a `url` is absolute.
    """
    return bool(urlparse(url).netloc)


def is_valid(url):
    """
    Checks whether `url` is a valid URL.
    """
    parsed = urlparse(url)
    return bool(parsed.netloc) and bool(parsed.scheme)


def get_all_images(url, http_proxy, cookies):
    """
    Returns all image URLs on a single `url`
    """
    urls = []
    # TODO handle the case of page not returned, i.e. response code != 200
    try:
        soup = bs(requests.get(url, proxies={'http': http_proxy, 'https': http_proxy}, cookies=cookies).content,
                  "html.parser", from_encoding="iso-8859-1")
        for img in soup.find_all("img"):
            img_url = img.attrs.get("src")

            if not img_url:
                # if img does not contain src attribute, just skip
                continue

            if not is_absolute(img_url):
                # if img has relative URL, make it absolute by joining
                img_url = urljoin(url, img_url)
            # remove URLs like '/hsts-pixel.gif?c=3.2.5'
            try:
                pos = img_url.index("?")
                img_url = img_url[:pos]
            except ValueError:
                pass
            # finally, if the url is valid
            if is_valid(img_url):
                urls.append(img_url)
    except Exception as e:
        utl.log_event(vr.download_log_file, vr.downloader, str(e))
    return urls


def download(url, pathname, http_proxy, cookies):
    """
    Downloads a file given an URL and puts it in the folder `pathname`
    """
    # if path doesn't exist, make that path dir
    if not os.path.isdir(pathname):
        os.makedirs(pathname)
    # TODO handle the case of page not returned, i.e. response code != 200
    try:
        # download the body of response by chunk, not immediately
        response = requests.get(url, proxies={'http': http_proxy, 'https': http_proxy}, cookies=cookies, stream=True)
    except Exception as e:
        utl.log_event(vr.download_log_file, vr.downloader, str(e))
        return

    # get the file name
    filename = os.path.join(pathname, url.split("/")[-1])

    try:
        with utl.safe_open_w(filename, "wb") as f:
            for data in response.iter_content(1024):
                # write data read to the file
                f.write(data)
    except Exception as e:
        utl.log_event(vr.download_log_file, vr.downloader, str(e))
        return


def get_image_from_page(url, path, proxy, cookies):
    # get all images
    imgs = get_all_images(url, proxy, cookies)
    for img in imgs:
        # for each img, download it
        download(img, path, proxy, cookies)
