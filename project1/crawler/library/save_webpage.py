import requests
from urllib.parse import urlparse
import random

from . import scrape_images as img, utils as utl, vars as vr


def get_and_save_webpage(url, direct, user_agent, http_proxy, n_cookies, cookies):
    """
    Given a url this routine downloads and saves the corresponding html and all the images contained in it.
    :param url: url of the webpage to be downloaded.
    :param direct: directory to save the webpage (html+ images)
    :param user_agent: user agent to be used in the headers of the http request.
    :param http_proxy: proxy to be used for the get request
    :param n_cookies: number of cookies series.
    :param cookies: list of cookies series. This is a list of dicts.
    :return: No return value.
    """
    domain_name = urlparse(url).netloc
    # path with file
    full_path = direct + url
    if 'https' in url:
        full_path = direct + url.replace('https://', '')
    elif 'http' in url:
        full_path = direct + url.replace('http://', '')

    # path without file
    full_dir_path = direct + domain_name
    for sub_url in urlparse(url).path.split('/')[:-1]:
        if sub_url == '':
            continue
        else:
            full_dir_path = full_dir_path + '/' + sub_url

    s = requests.Session()
    s.headers.update({
        'User-Agent': user_agent
    })

    # TODO handle the case of page not returned, i.e. response code != 200
    try:
        if n_cookies == 0:
            r = s.get(url, proxies={'http': http_proxy, 'https': http_proxy}, cookies=cookies, stream=True)
        else:
            cookie_index = random.randint(0, n_cookies - 1)
            r = s.get(url, proxies={'http': http_proxy, 'https': http_proxy}, cookies=cookies[cookie_index], stream=True)
    except Exception as e:
        utl.log_event(vr.download_log_file, vr.downloader, str(e))
        return

    # TODO: how do I handle directories and files having the same name?
    try:
        with utl.safe_open_w(full_path, 'w') as f:
            f.write(r.text)
    except Exception as e:
        utl.log_event(vr.download_log_file, vr.downloader, str(e))
        return

    if n_cookies == 0:
        img.get_image_from_page(url, full_dir_path, http_proxy, cookies)
    else:
        cookie_index = random.randint(0, n_cookies - 1)
        cookie = cookies[cookie_index]
        img.get_image_from_page(url, full_dir_path, http_proxy, cookie)


def download_summary(download_links_filename, download_summary_file_name, download_time):
    """
    This routine prints to file the summary of the downloading procedure, i.e. the total downloading time and
    the total number of downloaded links.
    :param download_links_filename: name of the file containing the list of all the downloaded links.
    :param download_summary_file_name: name of the file for saving the summary info.
    :param download_time: total downloading time.
    :return:
    """
    # Open the file with the list of all downloaded links and create a list that contains all of them
    with open(download_links_filename) as f:
        downloaded_links = f.read().split('\n')
    # Open/Create the file to save the summary info.
    f = utl.safe_open_w(download_summary_file_name, 'w')
    # Print to the summary file the total number of downloaded links (lenght of the downloaded_links list) and
    # the total downloading time.
    print(f'Total number of downloaded links: {len(downloaded_links)}', file=f)
    print(f'Total downloading time: {download_time}', file=f)
    # finally close the file.
    f.close()
