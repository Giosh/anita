import csv
import random
import time
import time as t
from urllib.parse import urlparse, urljoin

import pandas as pd
import requests
from bs4 import BeautifulSoup

from . import utils as utl, vars as vr


def is_valid(url):
    """
    Checks whether `url` is a valid URL.
    """
    parsed = urlparse(url)
    return bool(parsed.netloc) and bool(parsed.scheme)


def get_all_website_links(url, cookies, user_agent, http_proxy, log_file, domain_name):
    """
    Given a url, return all the internal links contained in the html, and the response code
    of the request.
    """
    # TODO retry for requests with errors
    # TODO still need to find a way to detect captchas --> ask to re-fresh cookies
    # initialize set for scraped urls
    urls = set()
    # skip this url if it is a 'log-out' link
    if 'logout' in url or 'signout' in url or 'sign out' in url or 'log out' in url:
        utl.log_event(vr.crawl_log_file, vr.crawler, 'Found log out or sign out in ' + url)
        return urls, 'None'
    else:
        try:
            # GET-REQUEST
            s = requests.Session()
            s.headers.update({
                'User-Agent': user_agent
            })
            s.proxies['http'] = http_proxy
            s.proxies['https'] = http_proxy
            web_page = s.get(url, cookies=cookies)

            # Make soup
            soup = BeautifulSoup(web_page.content, "html.parser", from_encoding="iso-8859-1")

            # Get all internal links
            for a_tag in soup.findAll("a"):
                href = a_tag.attrs.get("href")
                if href == "" or href is None:
                    # href empty tag
                    continue
                # join the URL if it's relative (not absolute link)
                href = urljoin(url, href)

                if not is_valid(href):
                    # not a valid URL
                    continue
                if href in urls:
                    # already in the set
                    continue
                if urlparse(url).netloc != domain_name:
                    # external link
                    continue
                urls.add(href)
        except Exception as e:
            utl.log_event(vr.crawl_log_file, vr.crawler, 'At ' + url + ' ' + str(e))
            urls.add('')
    if 'web_page' not in locals():
        utl.log_event(vr.crawl_log_file, vr.crawler, 'At ' + url + ' ' + 'Status code set to none')
        status_code = 'None'
    else:
        status_code = web_page.status_code
    return urls, status_code


def crawl(url, links, links_file_name, all_links_file_name, log_file, crawl_start, max_crawl_time, delay, n_cookies,
          cookies,
          user_agent, http_proxy, max_depth, domain_name, current_depth=0):
    """
    Given a url, crawl from there the website in a recursive way.
    """
    # Get current crawling time
    crawl_time = (t.time() - crawl_start) / 60

    # Update dynamic configs
    utl.update_config(vr.crawler + vr.internal_config_file, **{vr.crawl_time: crawl_time,
                                                               vr.current_depth: current_depth,
                                                               vr.n_crawled_links: len(links) + 1})

    # Do necessary checks
    if current_depth <= max_depth and url not in links and crawl_time < max_crawl_time:
        # add url to set
        links.add(url)
        # sleep before new request
        time.sleep(delay)
        # get links of url and response code
        if n_cookies == 0:
            get_results = get_all_website_links(url, cookies, user_agent, http_proxy, log_file, domain_name)
        else:
            cookie_index = random.randint(0, n_cookies - 1)
            get_results = get_all_website_links(url, cookies[cookie_index], user_agent, http_proxy, log_file,
                                                domain_name)
        internal_urls = get_results[0]
        res_code = get_results[1]

        # save url to file
        with utl.safe_open_w(links_file_name, 'a') as f:
            if urlparse(url).netloc == domain_name:
                try:
                    csv_writer = csv.writer(f, delimiter=',')
                    csv_writer.writerow([url, current_depth, res_code])
                except Exception as e:
                    utl.log_event(vr.crawl_log_file, vr.crawler, 'At ' + url + ' ' + str(e))

        # for each scraped link write it to file and crawl
        for link in internal_urls:
            with utl.safe_open_w(all_links_file_name, 'a') as f:
                try:
                    f.write('\n')
                    f.write(link)
                except Exception as e:
                    utl.log_event(vr.crawl_log_file, vr.crawler, 'At ' + url + ' ' + str(e))
            if urlparse(link).netloc == domain_name:
                crawl(link, links, links_file_name, all_links_file_name, log_file, crawl_start, max_crawl_time, delay,
                      n_cookies, cookies, user_agent, http_proxy, max_depth, domain_name,
                      current_depth=(current_depth + 1))


def crawl_summary(links_filename, all_links_filename, summary_file_name):
    """
    Print to file the total number of unique crawled links and the total
    crawling time.
    """
    links_df = pd.read_csv(links_filename)
    links_df.columns = ['url', 'depth', 'response_code']
    links = [x for x in links_df.url]

    with open(all_links_filename) as f1:
        all_links = f1.readlines()

    # write summary to summary file
    # with links counts and time
    f = utl.safe_open_w(summary_file_name, 'w')

    print('Total number of crawled links', len(all_links), file=f)
    print('Total number of unique crawled links', len(links), file=f)
    f.close()


def calculate_crawled_links_frequency(all_links_filename, freq_crawled_file_name):
    """
    At the end of crawling, given all the crawled links, compute for each crawled link
    how many times it has been crawled.
    """
    with open(all_links_filename) as f1:
        all_links = f1.readlines()

    # calculate links crawling frequency and save to file
    d = {}
    for link in all_links:
        d[link] = d.get(link, 0) + 1

    d = sorted(d.items(), key=lambda x: x[1], reverse=True)
    with utl.safe_open_w(freq_crawled_file_name, 'w') as f:
        for tlp in d:
            f.write(str(tlp))
            f.write('\n')


def calculate_n_links_per_l_depth(links_filename, freq_crawled_per_depth_file_name):
    """
    At the end of crawling, given all the crawled links, compute the count of links for
    each dept level and save the results to file.
    """
    links_df = pd.read_csv(links_filename)
    links_df.columns = ['url', 'depth', 'response_code']
    # calculate number of crawled links per level of depth and save to file
    counts_per_depth = links_df.groupby(['depth']).count()
    counts_per_depth.to_csv(freq_crawled_per_depth_file_name)
