import sys
import threading
import click

from crawl import start_crawler
from download import start_download
from display import display
from library import vars as vr
from library import utils as utl


@click.command()
@click.option('--cookies', type=click.Path(exists=True), default='No Cookies',
              help='Path to the file containing the cookies')
@click.argument('config', type=click.Path(exists=True))
def main(cookies, config):
    """
    This function contains the code that initiates the execution of the tool. It sets up
    and starts the threads.
    :param cookies: path to the file where the cookies are defined in a .csv format
    :param config: path to the configuration file that contains all the info about the
    website to the downloaded and the settings to do so, such as the proxy and local folder to use.
    :return: None
    """
    # todo check if config file follows the right template

    # ------------------------------------------------ Create internal configuration files
    utl.create_int_config_file(vr.internal_config_file)
    utl.create_int_config_file(vr.crawler + vr.internal_config_file)
    utl.create_int_config_file(vr.downloader + vr.internal_config_file)

    # --------------------------------------------------------Set first internal configurations
    utl.update_config(vr.internal_config_file, **{vr.path_to_config: config})

    if cookies != 'No Cookies':
        utl.update_config(vr.internal_config_file, **{vr.path_to_cookies: cookies})

    # --------------------------------------------------------Start Process
    # Set up directory to save the results
    utl.set_up_directory()

    # Define event to sync the threads
    crawl_event = threading.Event()
    download_event = threading.Event()

    # Start crawler
    try:
        crawler = threading.Thread(name='crawler', target=start_crawler, args=(crawl_event,))
        crawler.start()
    except Exception as e:
        utl.log_event(vr.global_log_file, vr.main, str(e))
        print('Program Exit. Check Log Files')
        sys.exit()

    # Start downloader
    try:
        downloader = threading.Thread(name='downloader', target=start_download, args=(crawl_event, download_event))
        downloader.start()
    except Exception as e:
        utl.log_event(vr.global_log_file, vr.main, str(e))
        print('Program Exit. Check Log Files')
        sys.exit()

    # Start display thread
    try:
        display_thread = threading.Thread(name='display', target=display, args=(crawl_event, download_event))
        display_thread.start()
    except Exception as e:
        utl.log_event(vr.global_log_file, vr.main, str(e))
        print('Program Exit. Check Log Files')
        sys.exit()


if __name__ == "__main__":
    main()
