from library import vars as vr, utils as utl
import time as t


def display(crawl_event, download_event):
    print("C Time   |C Links   |C Depth   |D Time     |D Links")

    t.sleep(20)
    while True:
        t.sleep(3)
        crawl_time = utl.get_config(vr.crawler + vr.internal_config_file, vr.crawl_time)
        n_crawled_links = utl.get_config(vr.crawler + vr.internal_config_file, vr.n_crawled_links)
        current_depth = utl.get_config(vr.crawler + vr.internal_config_file, vr.current_depth)
        download_time = utl.get_config(vr.downloader + vr.internal_config_file, vr.download_time)
        n_downloaded_links = utl.get_config(vr.downloader + vr.internal_config_file, vr.n_downloaded_links)

        print(f"{crawl_time:3.1f}\t|{n_crawled_links}\t    |     {current_depth}\t|{download_time:.1f} |{n_downloaded_links}", end="\r")

        if not download_event.isSet():
            break
