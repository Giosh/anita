import logging
import csv
import random
import time as t
import datetime as d 
import errno
import os.path
import os
import pandas as pd
import requests
from bs4 import BeautifulSoup
from urllib.parse import urlparse, urljoin


"""
    The crawler relies on 3 classes: 
    - RequestsSession: this object configures a Requests session with the required parameters
        needed to access the Dark Web .
    - Webpage: this class represents a webpage on the market that is being crawled. To initialize it 
        it is necessary to pass its url and a session object. 
    - Crawler: this represents the crawler it self that can be initialized and started
        and it will recursively crawl the specified Market up-until the two constraints (Time and Depth)
        are met. The object is created and used from the main of the crawler.  
      
"""

class Crawler: 

    def __init__(self, start_url, max_crawl_time, max_depth, request_session, folder_path):
        self.domain_name = urlparse(start_url).netloc
        self.start_url = start_url
        self.max_crawl_time = max_crawl_time
        self.max_depth = max_depth
        self.request_session = request_session
        self.folder_path = folder_path + '/' + str(d.datetime.now()).split(":", 1)[0].replace(' ', 'T')
        self.start_time = 0
        self.links = set()
    

    def set_start_time(self):
        # Set starting time of the crawler to be able to control the timing limit.
        self.start_time = t.time()


    def make_dir_for_saving(self):
        """
            Create the folder where the website will be saved at during the crawling.
        """
        Crawler.mkdir_p(self.folder_path)


    def crawl(self, url, current_depth):
        """
            Recursive function at the heart of the crawling strategy. 
        """
        current_time = (t.time() - self.start_time) / 60 
        cond1 = current_time < self.max_crawl_time
        cond2 = current_depth < self.max_depth
        cond3 = url not in self.links

        if cond1 and cond2 and cond3:
            
            print(url)
            
            if Webpage.check_url:
                try:
                    webpage = Webpage(url, self.request_session)
                    file_name = Crawler.clean_file_name(self.folder_path +'/'+ urlparse(url).netloc.replace('.onion','') + urlparse(url).path.replace('/', '_') + '.html')
                    f = Crawler.safe_open_w(file_name, 'w')
                    print(webpage.r.content, file=f)
                    scraped_links = webpage.get_internal_links(self.domain_name)
                    for link in scraped_links:
                        self.crawl(link, current_depth+1)
                except Exception as e: 
                    print(e)


    @staticmethod
    def clean_file_name(file_name):
        """
            Remove extension from file name. 
        """
        if 'jpg' in file_name:
            file_name = file_name.replace('.jpg', '')
        if 'php' in file_name: 
            file_name = file_name.replace('.php', '')
        if 'png' in file_name: 
            file_name = file_name.replace('.png', '')
        return file_name
        

    @staticmethod
    def safe_open_w(path, mode):
        """ Open "path" for writing, creating any parent directories as needed.
        """
        Crawler.mkdir_p(os.path.dirname(path))
        return open(path, mode)
        
    
    @staticmethod
    def mkdir_p(path):
        """ create the path"""
        try:
            os.makedirs(path)
        except OSError as exc:  # Python >2.5
            if exc.errno == errno.EEXIST and os.path.isdir(path):
                pass
            else:
                raise


class RequestsSession():
    def __init__(self, proxies, user_agent, cookies):
        self.proxies = proxies 
        self.user_agent = user_agent
        self.cookies = cookies

    def get_session(self):
        """
            This method retrieves a Requests object that can be used 
            throughout the crawling. 
        """
        s = requests.Session()
        s.headers.update({
                'User-Agent': self.user_agent
            })
        s.proxies.update(self.proxies)
        s.cookies.update(self.cookies)
        
        return s


class Webpage():

    def __init__(self, url, session):
        self.url = url
        self.session = session
        self.r = self.get_webpage()

    
    def get_webpage(self):
        """
            This returns a Response Object and Raises 
            requests.exceptions.HTTPError when status code is 404
        """
        r = self.session.get(self.url)
        r.raise_for_status()

        return r 


    def get_internal_links(self, domain_name):
        """
            Scrape all the links withing this webpage that belong to the domain 
            of the Market that is being crawled. 
        """
        urls = set()
        soup = BeautifulSoup(self.r.content, features="html5lib")
        for a_tag in soup.findAll("a"):
            href = a_tag.attrs.get("href")
            if href == "" or href is None:
               continue
            href = urljoin(self.url, href)
            if not Webpage.is_valid(href):
                continue
            if href in urls:
                continue
            if urlparse(href).netloc != domain_name:
                continue
            urls.add(href)
            
        return urls


    @staticmethod
    def is_valid(url):
        # Check if a url is valid
        parsed = urlparse(url)
        return bool(parsed.netloc) and bool(parsed.scheme)
    

    @staticmethod
    def check_url(url):
        # check if the url would lead to exit the website, or if it refers to images. 
        if 'logout' not in url or 'signout' not in url or 'sign out' not in url or 'log out' not in url or 'png' not in url or 'jpg' not in url:
            return True
        else:
            return False


        