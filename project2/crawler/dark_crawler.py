from lib.classes import *
import click
import csv

"""
The crawler has the following constraints set up: 
- Max Crawl Time (in minutes): 240
- Max Depth to reach: 9

Change these settings in the code if you want. 

"""

@click.command()
@click.argument('cookies_file', type=click.Path(exists=True))
@click.argument('url')
@click.argument('folder_path')
def main(cookies_file, url, folder_path):
    
    proxies = {'http': 'socks5h://localhost:9050'}
    user_agent = 'Mozilla/5.0 (Windows NT 10.0; rv:68.0) Gecko/20100101 Firefox/68.0'
    cookies = {}
    with open(cookies_file, newline='') as f:
        cookies_reader = csv.reader(f, delimiter=',')
        for line in cookies_reader:
            cookies[line[0]] = line[1]
    session = RequestsSession(proxies, user_agent, cookies).get_session()
    crawler = Crawler(url, 240, 9, session, folder_path)
    crawler.set_start_time()
    crawler.make_dir_for_saving()
    crawler.crawl(url, 0)


if __name__ == "__main__":
    main()
