# ANITA
## Description
This is a tool to bulk download websites on the dark web. The tool runs until the cookie session is valid. Please refer to the comments in the code to understand its structure. 
## Setup
##### 1.  Tor Service
The tool runs on **Ubuntu** and requires **Tor Service** to be installed on the machine. You can follow the instructions at [this link](https://miloserdov.org/?p=3837) to do so. 

You can check if the process is listening using command netstat.

``netstat -tupln``

Tor runs by default a SOCKS Proxy on port 9050. However, you can configure which proxy to use, as long as it supports get requests of .onion web pages. 

##### 2.  VPN 
It is better to install a **VPN** on the machine where you are running the tool. 

##### 3. Tor Browser
In order to get the cookies that are needed for the tool to run you will need to use the **Tor Browser**. 

##### 4. Installation 
Clone the repo to your local machine, and set up a Python interpreter by installing the modules in the *requirements.txt*
file. 
  
## Usage

After making sure that the Tor Service is up and running you can start the tool as follows: 

1. Open the Tor Browser and navigate to the home page of the website you want to download. To access most of the 
markets on the dark web you will need to fill in a captcha and log in. Therefore you need to register if you 
do not have an account, log in and fill in the captcha. Some websites have multiple captchas, make sure to fill 
in all of them to install a full access to the website. 

3. Open the developer console of the browser (Ctrl-Shift-I) and select the Network tab. Refresh the page and expand the
GET request of the web page. There you can access the Request Headers, that you can copy. They contain the cookies that
are needed to run the tool. 

4. Past the copied cookies in the *cookies.csv* file. Make sure to not include a header and for every 
row indicate the name of the cookie and its value. The delimiter is a comma. You can find an example of a cookie file
in the repo. Save the file. 

5. From the command line navigate to the *project2/crawler* folder of the repo.

6. Run the following command: 

    `python3 dark_crawler.py [path to cookies file] [url] [path to folder to save the website crawled]`

The tool is now running and at the end you will find the bulk downloaded website in the folder that you indicated
in the above command. While the tool runs it will display on the command line the links that it recursively crawls.

## Remarks

- Make sure to not close the Tor Browser during the whole duration of the process, as this will invalidate the cookies.
  
- To double check whether the process ended because it actually finished crawling+downloading the website you can 
reload the Tor webpage of the website when the program finished running. If reloading the page results in a captcha, then this
means that the program stopped because the cookies expired and not because it actually crawled + downloaded the whole site. 
Ultimately this means that you downloaded a part of the website but not all of it. 

- Even when the tool successfully finishes its tasks without the cookies expiring, it can still mean that it did not manage
to crawl and download part of the website. It likely covered the majority of it, but sometimes the request to a webpage
results in an error, which can be caused by the fact that in that moment the website is not
working (i.e. it id in down), or because the access to that specific page is blocked. When this happens
that is, when a GET request returns a code that is different from 200 and the tool does not 
receive the .html of that page, it will skip all the links contained in it. 
There is a high degree of randomness in this process, because nit can also be that such skipped links will be eventually reached 
anyways from other webpages that the tool easily retrieved. 

- The tool is not robust to abrupt loss of connection, hence make sure that you have a stable connection when running it.


