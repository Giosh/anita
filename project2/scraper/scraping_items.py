import os
from bs4 import BeautifulSoup
import pandas as pd
import requests
import datetime

def dump_to_df_items_of_all_vendors(pathFolder, dumpName):
    vendorList = []
    for filename in os.listdir(pathFolder):
        if filename.endswith(".html"): 
            if 'kid_vendor_' in filename:
                # Generate a list of vendors in the dataset
                vendorList.append([os.path.join(pathFolder, filename),filename])
        else:
            continue
        
    # Pre-define the structure of the final dataset
    columns = ['vendorname', 'title', 'stock', 'sales', 'from', 'origin', 'ships_to', 'views', 'age']
    df = pd.DataFrame(columns=columns)
    
    for vendor in vendorList:
        # Loop through all vendor that are available in the dataset
        with open(vendor[0], 'r') as f:
            contents = f.read()
            soup = BeautifulSoup(contents, 'html.parser')
            for item in soup.find_all(class_=['col-md-9']):
                # Loop through all items that are available for the vendor
                vendor_i = vendor[1].split("_")[2][:-5]
                title_i = item.find('h5').text
                stock_i = item.find("span", text="Stock:").next_sibling
                sales_i = item.find("span", text="Sales:").next_sibling
                from_i = item.find("span", text="From:").next_sibling
                origin_i = item.find("span", text="Origin:").next_sibling
                ships_to_i = item.find("span", text="Ships To:").next_sibling
                views_i = item.find("span", text="Views:").next_sibling
                age_i = item.find("span", text="Age:").next_sibling.strip()
                df = df.append({'vendorname' : vendor_i, 'title' : title_i, 'stock' : stock_i, 'sales' : sales_i, 'from' : from_i, 'origin' : origin_i, 'ships_to' : ships_to_i, 'views' : views_i, 'age' : age_i}, ignore_index=True)    
    df['time'] = datetime.datetime.strptime(dumpName, '%d-%m-%YT%H')
    return df


path = os.getcwd()+r'\monopoly'
directory_contents = os.listdir(path)
dumps = []
for filename in directory_contents:
        if filename.endswith(".py"): 
           continue
        else:
            dumps.append([os.path.join(path, filename), filename])

total = pd.DataFrame(columns=['time','vendorname', 'title', 'stock', 'sales', 'from', 'origin', 'ships_to', 'views', 'age'])
for i in dumps:
    total = total.append(dump_to_df_items_of_all_vendors(i[0], i[1]))
total.to_csv('totalItems.csv')
    