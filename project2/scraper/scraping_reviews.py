import os
from bs4 import BeautifulSoup
import pandas as pd
import datetime
import requests

def dump_to_df_reviews_of_all_vendors(pathFolder, dumpName):
    vendorList = []
    for filename in os.listdir(pathFolder):
        if filename.endswith(".html"): 
            if 'kid_vendor_' in filename:
                # Generate a list of vendors in the dataset
                vendorList.append([os.path.join(pathFolder, filename),filename])
        else:
            continue

    # Pre-define the structure of the final dataset
    columns = ['vendorname', 'rating', 'comment', 'date']
    df = pd.DataFrame(columns=columns)
    
    for vendor in vendorList:
        # Loop through all vendor that are available in the dataset
        with open(vendor[0], 'r') as f:
            contents = f.read()
            soup = BeautifulSoup(contents, 'html.parser')
            for item in soup.find_all(class_=['table table-striped']):
                # Loop through all reviews that are available for the vendor
                table_body = item.find('tbody')
                rows = table_body.find_all('tr')
                for row in rows:
                    cols = row.find_all('td')
                    cols = [ele.text.strip() for ele in cols]
                    data_row = [ele for ele in cols if ele]
                
                    vendor_i = vendor[1].split("_")[2][:-5]
                    rating_i = data_row[0]
                    comment_i = data_row[1]
                    date_i = data_row[2]

                    df = df.append({'vendorname' : vendor_i, 'rating' : rating_i, 'comment' : comment_i, 'date' : date_i}, ignore_index=True)    
    df['time'] = datetime.datetime.strptime(dumpName, '%d-%m-%YT%H')
    return df

path = os.getcwd()+r'\monopoly'
directory_contents = os.listdir(path)
dumps = []
for filename in directory_contents:
        if filename.endswith(".py"): 
           continue
        else:
            dumps.append([os.path.join(path, filename), filename])

total = pd.DataFrame(columns=['time', 'vendorname', 'rating', 'comment', 'date'])
for i in dumps:
    total = total.append(dump_to_df_reviews_of_all_vendors(i[0], i[1]))
    
total.to_csv('totalReviews.csv')