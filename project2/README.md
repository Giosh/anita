# Data Forensics

This project consists of three parts: 
1. **Crawler**: Similar to che crawler of Project1, this project contains a tool to bulk download websites on the dark web. The tool for now only works with websites that do not perform 
checks on the duration of the session established, that is, their cookies do not expire once created. 
2. **Scraper**: Python scripts to extract useful information from the crawled websites.
3. **Visualizations**: contains a Jupyter Notebook where the extrapolated information is further processed and vizualized. 
