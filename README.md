# Data Forensics - ANITA

Welcome to the repository of the Data Forensics Project. The main goal of this project is to crawl, scrape and analyse Illegal Drug Trafficking Markets on the Dark Web.

This project consists of two subprojects: 

- **Project1**: this contains the code of the crawler developed between January 2020 and July 2021 from Giorgia. 
- **Project2**: this contains the code and other materials of the crawler developed as part of the Data Forensics course from Giorgia and Lars. 

The folder **website_list** this is a utility folder used to keep track of all the
dark (and surface) sites used throughout the project.

Please refer to the README.md file of each sub-project for further details. 
